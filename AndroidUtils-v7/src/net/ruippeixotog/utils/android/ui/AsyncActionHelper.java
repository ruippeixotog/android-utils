package net.ruippeixotog.utils.android.ui;

import android.app.Activity;
import android.app.ProgressDialog;

public class AsyncActionHelper {

	private Activity activity;
	private ProgressDialog dialog;

	public AsyncActionHelper(Activity activity) {
		this.activity = activity;
	}

	public void runAsyncAction(final Runnable action, final Runnable onFinish) {

	}

	public void runAsyncAction(final Runnable action, final Runnable onFinish,
			int loadingTextResId) {
		runAsyncAction(action, onFinish, activity.getString(loadingTextResId),
				null);
	}

	public void runAsyncAction(final Runnable action, final Runnable onFinish,
			int loadingTextResId, int loadingTitleResId) {
		runAsyncAction(action, onFinish, activity.getString(loadingTextResId),
				activity.getString(loadingTitleResId));
	}

	public void runAsyncAction(final Runnable action, final Runnable onFinish,
			String loadingText, String loadingTitle) {
		if (loadingText != null)
			dialog = ProgressDialog.show(activity, loadingTitle, loadingText,
					true);
		else
			dialog = null;

		new Thread() {
			@Override
			public void run() {
				action.run();
				activity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if (onFinish != null)
							onFinish.run();
						if (dialog != null)
							dialog.dismiss();
					}
				});
			}
		}.start();
	}
}
