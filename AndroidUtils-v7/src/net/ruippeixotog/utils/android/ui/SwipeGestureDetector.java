package net.ruippeixotog.utils.android.ui;

import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;

public class SwipeGestureDetector extends SimpleOnGestureListener {

	private static final int SWIPE_MIN_DISTANCE = 120;
	private static final int SWIPE_MAX_OFF_PATH = 250;
	private static final int SWIPE_THRESHOLD_VELOCITY = 200;

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		// check if vertical movement is acceptably low for a fling
		if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
			return false;

		if (Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
			// right to left swipe
			if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE)
				onSwipeLeft(e1, e2, velocityX, velocityY);

			// left to right swipe
			else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE)
				onSwipeRight(e1, e2, velocityX, velocityY);
		}
		return false;
	}

	public boolean onSwipeLeft(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		return false;
	}

	public boolean onSwipeRight(MotionEvent e1, MotionEvent e2,
			float velocityX, float velocityY) {
		return false;
	}
}
