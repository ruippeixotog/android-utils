package net.ruippeixotog.utils.android.ui;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import android.app.TimePickerDialog;
import android.widget.EditText;
import android.widget.TimePicker;

public class EditTextTimeSetListener implements
		TimePickerDialog.OnTimeSetListener {

	private static DateFormat timeFormatter = new SimpleDateFormat("HH:mm");

	private EditText activeView = null;
	private Calendar activeCalendar = null;

	public EditTextTimeSetListener() {
	}

	public EditTextTimeSetListener(EditText activeView) {
		this.activeView = activeView;
	}

	public EditTextTimeSetListener(EditText activeView, Calendar activeCalendar) {
		this.activeView = activeView;
		this.activeCalendar = activeCalendar;
	}

	@Override
	public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
		Calendar chosenTime = new GregorianCalendar(0, 1, 1, hourOfDay, minute);

		if (activeCalendar != null)
			activeCalendar.setTime(chosenTime.getTime());

		if (activeView != null)
			activeView.setText(timeFormatter.format(chosenTime.getTime()));
	}

	public EditText getActiveView() {
		return activeView;
	}

	public void setActiveView(EditText activeView) {
		this.activeView = activeView;
	}

	public Calendar getActiveCalendar() {
		return activeCalendar;
	}

	public void setActiveCalendar(Calendar activeCalendar) {
		this.activeCalendar = activeCalendar;
	}

	public static DateFormat getTimeFormat() {
		return timeFormatter;
	}

	public static void setTimeFormat(DateFormat format) {
		timeFormatter = format;
	}
}
