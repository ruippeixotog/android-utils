package net.ruippeixotog.utils.android.ui;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import android.app.DatePickerDialog;
import android.widget.DatePicker;
import android.widget.EditText;

public class EditTextDateSetListener implements
		DatePickerDialog.OnDateSetListener {

	private static DateFormat dateFormatter = DateFormat
			.getDateInstance(DateFormat.MEDIUM);

	private EditText activeView = null;
	private Calendar activeCalendar = null;

	public EditTextDateSetListener() {
	}

	public EditTextDateSetListener(EditText activeView) {
		this.activeView = activeView;
	}

	public EditTextDateSetListener(EditText activeView, Calendar activeCalendar) {
		this.activeView = activeView;
		this.activeCalendar = activeCalendar;
	}

	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		Calendar chosenDate = new GregorianCalendar(year, monthOfYear,
				dayOfMonth);

		if (activeCalendar != null)
			activeCalendar.setTime(chosenDate.getTime());

		if (activeView != null)
			activeView.setText(dateFormatter.format(chosenDate.getTime()));
	}

	public EditText getActiveView() {
		return activeView;
	}

	public void setActiveView(EditText activeView) {
		this.activeView = activeView;
	}

	public Calendar getActiveCalendar() {
		return activeCalendar;
	}

	public void setActiveCalendar(Calendar activeCalendar) {
		this.activeCalendar = activeCalendar;
	}

	public static DateFormat getDateFormat() {
		return dateFormatter;
	}

	public static void setDateFormat(DateFormat format) {
		dateFormatter = format;
	}
}
