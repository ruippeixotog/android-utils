package net.ruippeixotog.utils.android.ui;

import java.util.Calendar;
import java.util.GregorianCalendar;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CalendarGridAdapter extends BaseAdapter {

	private Context context;
	private GregorianCalendar cal;
	private int cellOffset;

	private int cellResId;
	private String[] weekDayLabels = { "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa" };

	public CalendarGridAdapter(Context context, GregorianCalendar month,
			int cellResId) {
		this.context = context;
		setMonth(month);
		this.cellResId = cellResId;
	}

	public CalendarGridAdapter(Context context, GregorianCalendar month,
			int cellResId, String[] weekDayLabels) {
		this.context = context;
		setMonth(month);
		this.cellResId = cellResId;
		this.weekDayLabels = weekDayLabels;
	}

	public void setMonth(GregorianCalendar cal) {
		this.cal = cal;
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cellOffset = cal.get(Calendar.DAY_OF_WEEK) + 6;
	}

	@Override
	public int getCount() {
		return cellOffset + cal.getActualMaximum(Calendar.DAY_OF_MONTH);
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position - cellOffset;
	}

	public int getDayOfMonth(int position) {
		return (int) getItemId(position) + 1;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null)
			convertView = View.inflate(context, cellResId, null);

		if (position < 7) {
			String text = weekDayLabels[position];
			((TextView) convertView).setText(text);
		} else if (position >= cellOffset)
			convertView = getView(convertView, position % 7 + 1,
					(int) getItemId(position) + 1);
		else {
			((TextView) convertView).setText(null);
		}
		return convertView;
	}

	public View getView(View inflatedView, int weekDay, int dayOfMonth) {
		((TextView) inflatedView).setText(Integer.toString(dayOfMonth));
		return inflatedView;
	}
}
