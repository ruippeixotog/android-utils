package net.ruippeixotog.utils.android.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.util.AttributeSet;
import android.view.View;

public class GraphView extends View {

	// constants: graph types
	public static final int BAR = 0;
	public static final int LINE = 1;

	// constants: colors
	private static final int TEXT_COLOR = Color.BLACK;
	private static final int GRAPH_COLOR = Color.DKGRAY;
	private static final int DATA_COLOR = Color.LTGRAY;

	// constants: dimentions (in dip)
	private static final float TEXT_SIZE = 10.0f;
	private static final float TITLE_SIZE = 12.0f;

	private static final float BORDER_SIZE = 14.0f;
	private static final float LABEL_OFFSET = 3.0f;

	// constants: stroke width
	private static final float GRAPH_STROKE_WIDTH = 1.0f;
	private static final float LINE_STROKE_WIDTH = 2.0f;

	// data fields
	private String title;
	private String[] horLabels;
	private String[] verLabels;
	private float[] values;

	// visual config: graphic presentation
	private int type = BAR;
	private boolean beginAtOrigin = false;

	// visual config: vertical scale
	private boolean isFixedMin = false;
	private float fixedMin;
	private boolean isFixedMax = false;
	private float fixedMax;

	private boolean isIntegerScale = false;
	private float verOffsetRatio = 0.2f;

	// visual config: global view parameters
	private int reqViewWidth;
	private int reqViewHeight;

	// utilities
	private Paint paint = new Paint();

	public GraphView(Context context) {
		super(context);
		init();
		clear();
	}

	public GraphView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
		clear();
	}

	public GraphView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
		clear();
	}

	public GraphView(Context context, float[] values, String title,
			String[] horlabels, String[] verlabels, int type) {
		super(context);
		init();

		if (values == null)
			values = new float[0];
		else
			this.values = values;
		if (title == null)
			title = "";
		else
			this.title = title;
		if (horlabels == null)
			this.horLabels = new String[0];
		else
			this.horLabels = horlabels;
		if (verlabels == null)
			this.verLabels = new String[0];
		else
			this.verLabels = verlabels;
		this.type = type;
		paint = new Paint();
	}

	public void clear() {
		horLabels = verLabels = new String[0];
		values = new float[0];
		title = "";
	}

	private void init() {
		paint.setAntiAlias(true);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(int titleResId) {
		this.title = getContext().getString(titleResId);
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public float[] getValues() {
		return values;
	}

	public void setValues(float[] values) {
		this.values = values;
	}

	public String[] getHorizontalLabels() {
		return horLabels;
	}

	public void setHorizontalLabels(String[] horLabels) {
		this.horLabels = horLabels;
	}

	public String[] getVerticalLabels() {
		return verLabels;
	}

	public void setVerticalLabelsFromValues(int numLabels) {
		float max = getMax();
		if (isIntegerScale)
			setVerticalLabels(numLabels, (int) getMin(max, numLabels),
					(int) max);
		else
			setVerticalLabels(numLabels, getMin(max, numLabels), max);
	}

	private void setVerticalLabels(int numLabels, int minValue, int maxValue) {
		int diff = maxValue - minValue;
		int labelDiff = diff / (numLabels - 1);

		String[] verLabels = new String[numLabels];
		for (int i = 0; i < numLabels; i++) {
			verLabels[i] = Integer.toString(minValue + (numLabels - i - 1)
					* labelDiff);
		}
		setVerticalLabels(verLabels);
	}

	private void setVerticalLabels(int numLabels, float minValue, float maxValue) {
		float diff = maxValue - minValue;
		float labelDiff = diff / (numLabels - 1);

		String[] verLabels = new String[numLabels];
		for (int i = 0; i < numLabels; i++) {
			verLabels[i] = Float.toString(minValue + (numLabels - i - 1)
					* labelDiff);
		}
		setVerticalLabels(verLabels);
	}

	public void setVerticalLabels(String[] verLabels) {
		this.verLabels = verLabels;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public boolean doesBeginAtOrigin() {
		return beginAtOrigin;
	}

	public void setBeginAtOrigin(boolean beginAtOrigin) {
		this.beginAtOrigin = beginAtOrigin;
	}

	public float getFixedMininum() {
		return fixedMin;
	}

	public void setFixedMinimum(boolean isFixed, float fixedMin) {
		this.isFixedMin = isFixed;
		this.fixedMin = fixedMin;
	}

	public float getFixedMaximum() {
		return fixedMax;
	}

	public void setFixedMaximum(boolean isFixed, float fixedMax) {
		this.isFixedMax = isFixed;
		this.fixedMax = fixedMax;
	}

	public boolean isIntegerScale() {
		return isIntegerScale;
	}

	public void setIntegerScale(boolean integerScale) {
		this.isIntegerScale = integerScale;
	}

	public float getVerticalOffsetRatio() {
		return verOffsetRatio;
	}

	public void setVerticalOffsetRatio(float verOffsetRatio) {
		this.verOffsetRatio = verOffsetRatio;
	}

	public int getRequestedWidth() {
		return reqViewWidth;
	}

	public void setRequestedWidth(int width) {
		this.reqViewWidth = width;
	}

	public int getRequestedHeight() {
		return reqViewHeight;
	}

	public void setRequestedHeight(int height) {
		this.reqViewHeight = height;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		float border = toPixels(BORDER_SIZE);
		float horStart = border * 2;
		float height = getHeight();
		float width = getWidth() - 1;
		float max = getMax();
		float min = getMin(max);
		float diff = max - min;
		float graphHeight = height - (2 * border);
		float graphWidth = width - (2 * border);
		int labelOffset = toPixels(LABEL_OFFSET);

		paint.setTextSize(toPixels(TEXT_SIZE));
		paint.setStrokeWidth(GRAPH_STROKE_WIDTH);
		paint.setAntiAlias(true);

		// draw vertical labels and horizontal lines
		paint.setTextAlign(Align.LEFT);
		int vers = verLabels.length - 1;
		for (int i = 0; i < verLabels.length; i++) {
			float y = ((graphHeight / vers) * i) + border;

			paint.setColor(Color.DKGRAY);
			canvas.drawLine(horStart, y, width, y, paint);

			paint.setColor(TEXT_COLOR);
			canvas.drawText(verLabels[i], 0, y, paint);
		}

		// draw horizontal labels and vertical lines
		int hors = horLabels.length - 1;
		float labelSize = graphWidth / (beginAtOrigin ? hors : hors + 1);
		for (int i = 0; i < horLabels.length; i++) {

			// calculate slice beginning
			float x = horStart + i * labelSize;

			// paint horizontal line
			paint.setColor(GRAPH_COLOR);
			canvas.drawLine(x, height - border, x, border, paint);

			// draw label
			paint.setTextAlign(Align.CENTER);
			if (beginAtOrigin) {
				if (i == horLabels.length - 1)
					paint.setTextAlign(Align.RIGHT);
				else if (i == 0)
					paint.setTextAlign(Align.LEFT);
			}

			/*Path path = new Path();
			path.lineTo(labelSize, 0);*/

			paint.setColor(TEXT_COLOR);
			canvas.drawText(horLabels[i],
					beginAtOrigin ? x : x + labelSize / 2,
					height - labelOffset, paint);

			/*Path path = new Path();
			path.moveTo(beginAtOrigin ? x : x + labelSize / 2, height
					- labelOffset);
			path.lineTo((beginAtOrigin ? x : x + labelSize / 2) + labelSize,
					height - labelOffset);

			paint.setColor(TEXT_COLOR);
			canvas.drawTextOnPath(horLabels[i], path, 0, 0, paint);*/
		}

		// draw off-by-one line at the end, if data begins at origin
		if (!beginAtOrigin) {
			paint.setColor(GRAPH_COLOR);
			canvas.drawLine(horStart + graphWidth, height - border, horStart
					+ graphWidth, border, paint);
		}

		// draw title
		paint.setTextSize(toPixels(TITLE_SIZE));
		paint.setTextAlign(Align.CENTER);
		canvas.drawText(title, (graphWidth / 2) + horStart, border
				- labelOffset, paint);

		// draw values
		if (max != min) {
			paint.setColor(DATA_COLOR);
			paint.setStrokeWidth(LINE_STROKE_WIDTH);

			float colWidth = (width - 2 * border)
					/ (beginAtOrigin ? values.length - 1 : values.length);
			float halfCol = beginAtOrigin ? 0 : colWidth / 2;
			float lastH = 0;

			for (int i = 0; i < values.length; i++) {
				float val = values[i] - min;
				float rat = val / diff;
				float h = graphHeight * rat;

				if (type == BAR) {
					canvas.drawRect(horStart + i * colWidth, (border - h)
							+ graphHeight, (horStart + i * colWidth)
							+ (colWidth - 1), height - (border - 1), paint);
				} else {
					if (i > 0)
						canvas.drawLine((colWidth * (i - 1)) + (horStart + 1)
								+ halfCol, (border - lastH) + graphHeight,
								(i * colWidth) + (horStart + 1) + halfCol,
								(border - h) + graphHeight, paint);
					lastH = h;
				}
			}
		}
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		int widthSize = MeasureSpec.getSize(widthMeasureSpec);
		int heightSize = MeasureSpec.getSize(heightMeasureSpec);

		int width = (widthMode == MeasureSpec.EXACTLY) ? widthSize
				: (widthMode == MeasureSpec.AT_MOST) ? Math.min(widthSize,
						reqViewWidth) : reqViewWidth;
		int height = (heightMode == MeasureSpec.EXACTLY) ? heightSize
				: (heightMode == MeasureSpec.AT_MOST) ? Math.min(heightSize,
						reqViewHeight) : reqViewHeight;
		setMeasuredDimension(width, height);
	}

	private float getMax() {
		if (isFixedMax)
			return fixedMax;
		float largest = Integer.MIN_VALUE;
		for (int i = 0; i < values.length; i++)
			if (values[i] > largest)
				largest = values[i];
		return largest;
	}

	private float getMin(float max) {
		return getMin(max, verLabels.length);
	}

	private float getMin(float max, int numLabels) {
		if (isFixedMin)
			return fixedMin;
		float smallest = Float.MAX_VALUE;
		for (int i = 0; i < values.length; i++)
			if (values[i] < smallest)
				smallest = values[i];
		smallest = smallest - verOffsetRatio * (max - smallest);

		// if it's an integer scale, adjust minValue so that labels represent
		// equally spaced integers
		if (isIntegerScale) {
			int labelDiff = (int) Math.ceil((max - smallest)
					/ (double) (numLabels - 1));
			smallest = max - labelDiff * (numLabels - 1);
		}
		return smallest;
	}

	private int toPixels(float dip) {
		final float scale = getResources().getDisplayMetrics().density;
		return (int) (dip * scale + 0.5f);
	}
}
